# twimey-android

Manage your wibbly-wobbly timey-wimey stuff.

Link Google calendars to your timeline.
Remove stuff that you don't care about.
Understand when you need to leave to get where you need to go.
Be notified when it's time to leave.

Like midu (which is now dead), but better.